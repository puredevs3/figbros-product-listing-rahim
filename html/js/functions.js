jQuery(function($){
    $('.home-banner-slider').slick({
        infinite: true,
        arrows: true,
        autoplay: true,
        dots:false,
        autoplaySpeed: 7000,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: '<div class="slick-custom-arrow slick-custom-arrow-right"><a href="#" ><img src="images/right-angle.png" alt="" class="img-fluid d-block"></a></div>',
        prevArrow: '<div class="slick-custom-arrow slick-custom-arrow-left"><a href="#" ><img src="images/left-angle.png" alt="" class="img-fluid d-block"></a></div>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
            }
        ]
    });
    var numberOfItems=4;

    $('#myList li:lt('+numberOfItems+')').show();

    $('#loadMore').click(function () {
        numberOfItems = numberOfItems+6;
        $('#myList li:lt('+numberOfItems+')').show();
        $('#loadMore').css("display", "none");
        $('#showLess').css("display", "block");
    });

    $('#showLess').click(function () {
        numberOfItems= numberOfItems-6;
        $('#myList li').not(':lt('+numberOfItems+')').hide();
        $('#loadMore').css("display", "block");
        $('#showLess').css("display", "none");
    });

});

